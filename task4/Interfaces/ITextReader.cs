﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task4.Interfaces
{
	public interface ITextReader
	{
		List<List<char>> ReadText(string filename);
	}
}
