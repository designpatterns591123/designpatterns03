﻿using task1.Interfaces;
using task1.Models.Adapters;
using task1.Models;

ILogger consoleLogger = new Logger();

consoleLogger.Log("This is a log message.");
consoleLogger.Error("This is an error message.");
consoleLogger.Warn("This is a warning message.");

using (FileLoggerAdapter fileLogger = new())
{
	fileLogger.Log("This is a log message for file.");
	fileLogger.Error("This is an error message for file.");
	fileLogger.Warn("This is a warning message for file.");
}

Console.WriteLine("Logs have been written to the console and file.");