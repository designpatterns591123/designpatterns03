﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task1.Interfaces;

namespace task1.Models.Adapters
{
	public class FileLoggerAdapter : ILogger, IDisposable
	{
		private readonly FileWriter _fileWriter;

		public FileLoggerAdapter()
		{
			_fileWriter = new FileWriter("log.txt");
		}

		public void Log(string message)
		{
			_fileWriter.WriteLine($"Log: {message}");
		}

		public void Error(string message)
		{
			_fileWriter.WriteLine($"Error: {message}");
		}

		public void Warn(string message)
		{
			_fileWriter.WriteLine($"Warn: {message}");
		}

		public void Dispose()
		{
			_fileWriter.Dispose();
		}
	}
}
