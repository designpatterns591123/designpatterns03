﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task5.Models
{
	public class LightHtmlFactory
	{
		private readonly Dictionary<string, LightHtmlElement> elements;

		public LightHtmlFactory()
		{
			elements = [];
		}

		public LightHtmlElement GetElement(string content)
		{
			if (string.IsNullOrWhiteSpace(content))
			{
				return null;
			}
			else if (content.StartsWith(" "))
			{
				return new LightHtmlElementNode("blockquote");
			}
			else if (content.Length < 20)
			{
				return new LightHtmlElementNode("h2");
			}
			else
			{
				return new LightHtmlElementNode("p");
			}
		}

		public LightHtmlElement GetTextNode(string content)
		{
			return new LightHtmlTextNode(content);
		}
	}
}
