﻿using task2.Models.Heroes;
using task2.Models.Inventory;
using task2.Models;

Hero warrior = new Warrior();
Hero mage = new Mage();
Hero paladin = new Paladin();


warrior.SetInventory(new Weapon());
warrior.SetInventory(new Armor());
mage.SetInventory(new Amulet());
paladin.SetInventory(new Weapon());
paladin.SetInventory(new Armor());
paladin.SetInventory(new Amulet());


warrior.Display();
warrior.EquipInventory();

mage.Display();
mage.EquipInventory();

paladin.Display();
paladin.EquipInventory();