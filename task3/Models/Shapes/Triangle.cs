﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task3.Interfaces;

namespace task3.Models.Shapes
{
	public class Triangle(IRenderer renderer) : Shape(renderer)
	{
		public override void Draw()
		{
			renderer.RenderShape("Triangle");
		}
	}
}
